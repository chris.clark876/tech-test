const { getPrompt } = require('./prompt-utils')
const { toTable } = require('./utils');

const LENGTH = 10;

const strategies = {
    "equal": val => val,
    "more-odd": ( val, index ) => {
        if(index % 2 === 1) {
            return val - val / 3;
        }
        return val + val / 3;
    }
}

const getAmount = (amount, strategy, length) => {
    let total = 0;
    const dividedPayment = amount / length

    const splitPayments = Array.from({length: length - 1}, ( _, index ) => {
        const payment = strategies[strategy](dividedPayment, index);
        const roundedPayment = Math.floor(payment * 100) / 100;

        total += roundedPayment;
        return roundedPayment;
    });

    if(total < amount) {
        const totalToBeAdded = amount - total;
        splitPayments[length - 1] = Math.floor(totalToBeAdded * 100) / 100;
    }

    return splitPayments.map(splitPayment => `$${splitPayment}`)
}


(async () => {
    const [amount, split] = await getPrompt();
    const splitPayments = getAmount(amount, split, LENGTH);

    console.table(toTable(splitPayments, amount));
})();

