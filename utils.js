/**
 * This just formats the array into a "pretty" format for console.table()
 */
const toTable = (array, tableKey) => {
    const tableObject = {};
    array.forEach((val, index) => tableObject[index + 1] = val)
    return { [ tableKey ]: tableObject };
}

module.exports = { toTable }