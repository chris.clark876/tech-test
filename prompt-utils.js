const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

const promptProperties = [
    {
        prompt: 'Please enter an amount (It must be a number): ',
        errorMessage: 'Amount requires a positive number',
        validator: val => Number(val) && val > 0,        
    },
    {
        prompt: 'Please enter a split: ',
        errorMessage: 'The split value must be "equal" or "more-odd"',
        validator: val => val === 'equal' || val === 'more-odd',        
    }
]

const readInput = (prompt) => new Promise((res, rej) => {
    readline.question(prompt, (answer) => {
        res(answer);
    })
})

const getPrompt = async () => {
    const promptValues = [];
    for (let i = 0; i < promptProperties.length; i++) {
        const promptProperty = promptProperties[i];

        const { prompt, validator, errorMessage } = promptProperty;
        const input = await readInput(prompt);

        if(!validator(input)) {
            i -= 1
            console.log('\x1b[1;31m%s\x1b[0m', errorMessage);
            continue;
        };

        promptValues.push(input);
      }
      readline.close();
      return promptValues;
}

module.exports = {
    getPrompt
}